﻿using System;

namespace Melyakina.L4.N2
{
    class Program
    {
        
            static double Method(double k)
            {
                double s = Math.Sqrt(k) / -2;
                return s;
            }
            static double Rec(double k)
            {


                if (k == 0)
                {
                    return Method(k);
                }
                else
                {
                    return Method(k) + Rec(k - 1);
                }


            }
            static void Main(string[] args)
            {


                int n = Convert.ToInt32(Console.ReadLine());
                double s = Rec(n);


                Console.WriteLine(s);
                Console.ReadKey();
            }
        }
    }

