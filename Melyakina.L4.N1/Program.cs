﻿using System;

namespace Melyakina.L4.N1
{
    class Program
    {
        
            //a. 5 + 30x
            //b. −y − 13/3


            static void Main(string[] args)
            {


                int x = Convert.ToInt32(Console.ReadLine());
                int y = Convert.ToInt32(Console.ReadLine());

                int c = ResInt(x, y);

                Console.WriteLine(c);


                double t = double.Parse(Console.ReadLine());
                double d = double.Parse(Console.ReadLine());

                double r = ResDouble(t, d);

                Console.WriteLine(r);
                




                char l = char.Parse(Console.ReadLine());
                char n = char.Parse(Console.ReadLine());

                int s = ResChar(l, n);

                Console.WriteLine(s);




            }

            //методы для типа  int

            static int Result1(int x)
            {
                int m = 5 + 30 * x;
                return m;
            }
            static int Result2(int y)
            {
                int m = -y - 13 / 3;
                return m;
            }
            static int ResInt(int x, int y)
            {
                int a = Result1(x);
                int b = Result2(y);
                int m = Math.Max(a, b);
                return m;
            }

            //методы для типа double

            static double Res1(double d)
            {
                double m = 5 + 30 * d;
                return m;
            }
            static double Res2(double w)
            {
                double m = -w - (13 / 3);
                return m;
            }
            static double ResDouble(double d, double w)
            {
                double a = Res1(d);
                double b = Res2(w);
                double m = Math.Max(a, b);
                return m;
            }

            //методы для типа char
            static int R1(char e)
            {
                int k = 5 + 30 * e;
                return k;
            }
            static int R2(char q)
            {
                int j = -q - (13 / 3);
                return j;
            }


            static int ResChar(char e, char q)
            {
                int c = R1(e);
                int d = R2(q);
                int l = Math.Max(c, d);
                return l;
            }
        }
    }

