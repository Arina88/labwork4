# Лабораторная работа 4 "Методы"

_Вариант 9_

**Задание 1**


    Реализовать метод, который производит операцию над числами x и y по формуле (a) и (b), после
    чего возвращает наибольшее из полученных значений. Входные параметры метода – два целых
    числа. Сделать перегрузку метода для параметров типа double. Сделать перегрузку метода для
    параметров типа char.
    a. 5 + 30x
    b. −y − 13/3

**Задание 2**

    Реализовать метод для вычисления значения функции с помощью рекурсивного подхода.
           n
    f(n) = ∑    √k
          k=1   −2


**Задание 3**
    
    Реализовать метод для вычисления значения функции с помощью рекурсивного подхода.

       n     (n+1)
     П k=1    15
    
